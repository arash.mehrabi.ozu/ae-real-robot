from ae_train import create_model
from glob import glob
from ae_test import test_single_image
from PIL import Image
# from kneed import KneeLocator
from sklearn.cluster import KMeans
import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt
import os

latent_size = 64
model_adr = './result_rot_old/64/ae_model_100.pt'
data_dir = './data/'


def get_all_latents():
    all_latents = []
    for d_dirs in glob(data_dir + "*"):
        for d_dir in glob(d_dirs):
            #print d_dir
            all_imgs_adr = d_dir + "/*.jpg"
            for img_adr in glob(all_imgs_adr):
                img = Image.open(img_adr)
                latent_rep = test_single_image(img, ae_model)
                latent_rep = latent_rep.squeeze(dim=0)
                latent_rep = latent_rep.cpu().detach().numpy()
                all_latents.append(latent_rep)

    print "num latents", len(all_latents)
    all_latents = np.array(all_latents)
    return all_latents


def get_kmeans_model(all_latents):
    kmeans_kwargs = {
        "init": "random",
        "n_init": 10,
        "max_iter": 300,
    }
    # # A list holds the SSE values for each k
    sse = []
    km_models = []
    for k in range(2, 11):
        kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
        kmeans.fit(all_latents)
        km_models.append(kmeans)
        sse.append(kmeans.inertia_)
    # kl = KneeLocator(
    #     range(2, 11), sse, curve="convex", direction="decreasing"
    # )
    return km_models #, kl


def plot_centers(km_models, ae_model, save_root):
    for km_model in km_models:
        km_model_centers = torch.from_numpy(km_model.cluster_centers_)
        with torch.no_grad():
            reconstructed_imgs_from_lr, latent_rep = ae_model(km_model_centers, only_decoder=True)

        # Plotting
        imgs = torch.stack([reconstructed_imgs_from_lr], dim=1).flatten(0, 1)
        grid = torchvision.utils.make_grid(imgs, nrow=3, normalize=True, range=(0, 1))
        grid = grid.permute(1, 2, 0)
        plt.figure(figsize=(24, 18))
        title_str = "Reconstructed images from later representation's centers of cluster with n_clusters = {}".format(km_model.cluster_centers_.shape[0])
        plt.title(title_str)
        plt.imshow(grid)
        plt.axis('off')
        # plt.show()
        plt.savefig(save_root + 'no_clusters_' + str(km_model.n_clusters))

        print "Done " + title_str


if __name__ == '__main__':
    ae_model = create_model(latent_size, model_adr)
    all_latents = get_all_latents()
    km_models = get_kmeans_model(all_latents)

    save_root = './result_rot_old/kmeans/' + str(latent_size) + '/'
    if not os.path.exists(save_root):
        os.makedirs(save_root)

    plot_centers(km_models, ae_model, save_root)
