# Python 2

from torch import nn

class Encoder(nn.Module):

    def __init__(self, num_input_channels, latent_dim):
        """
        Inputs:
            - num_input_channels : Number of input channels of the image. For CIFAR, this parameter is 3
            - latent_dim : Dimensionality of latent representation z
            - act_fn : Activation function used throughout the encoder network
        """
        super(Encoder, self).__init__()
        c_hid = 32
        self.net = nn.Sequential(
            nn.Conv2d(num_input_channels, c_hid, kernel_size=3, padding=1, stride=2),  # 120x120 => 60x60
            nn.ReLU(),
            nn.Conv2d(c_hid, c_hid, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(c_hid, 2 * c_hid, kernel_size=3, padding=1, stride=2),  # 60x60 => 30x30
            nn.ReLU(),
            nn.Conv2d(2 * c_hid, 2 * c_hid, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(2 * c_hid, 2 * c_hid, kernel_size=3, padding=1, stride=2),  # 30x30 => 15x15
            nn.ReLU(),
            nn.Flatten(),  # Image grid to single feature vector
            nn.Linear(2 * c_hid * (30 * 30), latent_dim)  # 2*c_hid is the number of output kernels.
        )

    def forward(self, x):
        y = self.net(x)
        return y

class Decoder(nn.Module):

    def __init__(self, num_input_channels, latent_dim):
        """
        Inputs:
            - num_input_channels : Number of channels of the image to reconstruct. For CIFAR, this parameter is 3
            - latent_dim : Dimensionality of latent representation z
            - act_fn : Activation function used throughout the decoder network
        """
        super(Decoder, self).__init__()
        c_hid = 32
        self.linear = nn.Sequential(
            nn.Linear(latent_dim, 2 * c_hid * 30 * 30),
            nn.ReLU()
        )
        self.net = nn.Sequential(
            nn.ConvTranspose2d(2 * c_hid, 2 * c_hid, kernel_size=3, output_padding=1, padding=1, stride=2),
            # 30x30 => 60x60
            nn.ReLU(),
            nn.Conv2d(2 * c_hid, 2 * c_hid, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.ConvTranspose2d(2 * c_hid, c_hid, kernel_size=3, output_padding=1, padding=1, stride=2),
            # 60x60 => 120x120
            nn.ReLU(),
            nn.Conv2d(c_hid, c_hid, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.ConvTranspose2d(c_hid, num_input_channels, kernel_size=3, output_padding=1, padding=1, stride=2),
            # 120x120 => 240x240
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.linear(x)
        x = x.reshape(x.shape[0], -1, 30, 30)
        x = self.net(x)
        return x


class Autoencoder(nn.Module):

    def __init__(self, latent_dim, num_input_channels=3):
        super(Autoencoder, self).__init__()
        # Creating encoder and decoder
        self.encoder = Encoder(num_input_channels, latent_dim)
        self.decoder = Decoder(num_input_channels, latent_dim)

    def forward(self, x, only_decoder=False):
        """
        The forward function takes in an image and returns the reconstructed image
        """
        if not only_decoder:
            latent_rep = self.encoder(x)
        else:
            latent_rep = x

        x_hat = self.decoder(latent_rep)
        return x_hat, latent_rep
