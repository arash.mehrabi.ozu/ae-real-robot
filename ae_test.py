import torch
from torchvision import transforms
from ae_train import create_model

def preprocess_frame(im):
    # Size of the image in pixels (size of original image)
    # (This is not mandatory)
    width, height = im.size

    # Setting the points for cropped image
    left = 0.50 * width
    right = width
    bottom = height
    top = 0.40 * height
    # Cropped image of above dimension
    # (It will not change original image)
    im1 = im.crop((left, top, right, bottom))

    return im1

def test_single_image(img, ae_model):
    data_transform = transforms.Compose([
        transforms.Resize([240, 240]),
        transforms.ToTensor(),
    ])
    x = data_transform(img)
    # make it a batch-like
    x = x.unsqueeze(dim=0)

    with torch.no_grad():
        reconstructed_img, latent_rep = ae_model(x)

    return latent_rep



