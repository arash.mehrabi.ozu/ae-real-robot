from k_nearest_neighbor import KNearestNeighbor
from ae_train import create_model
from ae_test import test_single_image
from PIL import Image

print "Creating test image's AE:"
model_adr = './result_rot_old/128/ae_model_95.pt'  # You can change this
latent_size = 128
ae_model = create_model(latent_size, model_adr)


test_img_adr = './data/humop_20220831_122402_vid/frame86.jpg'
# humop_20220831_122035_vid/213
# 914
# humop_20220831_122402_vid/86 l
# 129 r
# 1003 s
test_img = Image.open(test_img_adr)
latent_rep = test_single_image(test_img, ae_model)
# latent_rep = latent_rep.squeeze(dim=0)
latent_rep = latent_rep.cpu().detach().numpy()

knn = KNearestNeighbor()
print "Predicted class for the test image:", knn.predict(latent_rep)
