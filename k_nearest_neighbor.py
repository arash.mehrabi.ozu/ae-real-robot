import numpy as np
import glob
from ae_train import create_model
from ae_test import test_single_image
from PIL import Image
import torchvision
import matplotlib.pyplot as plt
import torch

root_dir = './classes/'
model_adr = './result_rot_old/128/ae_model_95.pt'  # You can change this
latent_size = 128
ae_model = create_model(latent_size, model_adr)

class KNearestNeighbor():
    def __init__(self):
        all_class_dirs = glob.glob(root_dir + '*')
        num_classes = len(all_class_dirs)
        X_train = np.empty((num_classes, latent_size))
        y_train = []
        class_counter = 0
        for class_dir in all_class_dirs:
            latent_rep_sum = 0
            num_samples = 0
            for img_adr in glob.glob(class_dir + '/*.jpg'):
                img = Image.open(img_adr)
                latent_rep = test_single_image(img, ae_model)
                # latent_rep = latent_rep.squeeze(dim=0)
                latent_rep_sum += latent_rep
                num_samples += 1

            latent_rep_sum /= num_samples
            latent_rep_sum = latent_rep_sum.squeeze(dim=0)
            class_name = class_dir.split("/")[-1]

            X_train[class_counter] = latent_rep_sum.cpu().detach().numpy()
            y_train.append(class_name)

            class_counter += 1

        self.X_train = X_train
        self.y_train = y_train

    def predict(self, X):
        """
        Predict labels for test data using this classifier.
        Inputs:
        - X: A numpy array of shape (num_test, D) containing test data consisting
             of num_test samples each of dimension D.
        Returns:
        - y: A numpy array of shape (num_test,) containing predicted labels for the
          test data, where y[i] is the predicted label for the test point X[i].
        """
        dists = self.compute_distances_two_loops(X)

        return self.predict_labels(dists)

    def compute_distances_two_loops(self, X):
        """
        Compute the distance between each test point in X and each training point
        in self.X_train using a nested loop over both the training data and the
        test data.
        Inputs:
        - X: A numpy array of shape (num_test, D) containing test data.
        Returns:
        - dists: A numpy array of shape (num_test, num_train) where dists[i, j]
          is the Euclidean distance between the ith test point and the jth training
          point.
        """
        num_test = X.shape[0]
        num_train = self.X_train.shape[0]
        dists = np.zeros((num_test, num_train))
        for i in range(num_test):
            for j in range(num_train):
                dists[i, j] = np.sqrt(np.sum(np.square(X[i] - self.X_train[j])))
                # print "dist from " + self.y_train[j] + " " + str(np.sqrt(np.sum(np.square(X[i] - self.X_train[j]))))

        return dists

    def predict_labels(self, dists):
        """
        Given a matrix of distances between test points and training points,
        predict a label for each test point.
        Inputs:
        - dists: A numpy array of shape (num_test, num_train) where dists[i, j]
          gives the distance betwen the ith test point and the jth training point.
        Returns:
        - y: A numpy array of shape (num_test,) containing predicted labels for the
          test data, where y[i] is the predicted label for the test point X[i].
        """
        num_test = dists.shape[0]
        y_pred = []
        for i in range(num_test):
            sorted_indices = np.argsort(dists[i])
            nearest_indice = sorted_indices[:1]
            y_pred.append(self.y_train[nearest_indice[0]])

        return y_pred

    def plot_means(latent_rep_sum):
        with torch.no_grad():
            reconstructed_imgs_from_lr, latent_rep = ae_model(latent_rep_sum, only_decoder=True)

        # Plotting
        imgs = torch.stack([reconstructed_imgs_from_lr], dim=1).flatten(0, 1)
        grid = torchvision.utils.make_grid(imgs, nrow=3, normalize=True, range=(0, 1))
        grid = grid.permute(1, 2, 0)
        plt.figure(figsize=(24, 18))
        title_str = "Reconstructed images from mean of the class " + class_dir
        plt.title(title_str)
        plt.imshow(grid)
        plt.axis('off')
        plt.show()

