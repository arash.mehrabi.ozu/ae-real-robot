from __future__ import division
import torch
from torch import nn
from autoencoder import Autoencoder
from torchvision import transforms, utils, datasets
import matplotlib.pyplot as plt
from torchvision.utils import make_grid
import os

def get_loader():
    data_transform = transforms.Compose([
        transforms.Resize([240, 240]),
        transforms.RandomRotation(degrees=(-20, 20)),
        # transforms.RandomHorizontalFlip(),
        # transforms.RandomVerticalFlip(p=0.5),
        transforms.ToTensor(),
        # transforms.Normalize(mean=[0.485, 0.456, 0.406],
        #                      std=[0.229, 0.224, 0.225]),
    ])

    dataset = datasets.ImageFolder(root='./data/',
                                               transform=data_transform)

    train_set, test_set = torch.utils.data.random_split(dataset, [6500, 451])

    trainaset_loader = torch.utils.data.DataLoader(train_set,
                                                   batch_size=8, shuffle=True)

    testset_loader = torch.utils.data.DataLoader(test_set,
                                                 batch_size=8, shuffle=False)

    return trainaset_loader, testset_loader

def create_model(latent_size, model_adr=None):
    ae_model = Autoencoder(latent_size)
    if not model_adr is None:
        ae_model.load_state_dict(torch.load(model_adr))
        print "Model weights loaded."
    return ae_model

def train(latent_size, model_adr=None, start_epoch=0):
    ae_model = create_model(latent_size, model_adr)
    trainaset_loader, testset_loader = get_loader()
    # Mean Squared Error
    criterion = nn.MSELoss()
    criterion_te = nn.MSELoss()
    optimizer = torch.optim.Adam(ae_model.parameters(), lr=3e-4)

    latent_size_str = str(latent_size)
    save_root = './result_rot_old/{}/'.format(latent_size_str)
    if not os.path.exists(save_root):
        os.makedirs(save_root)
        
    n_epochs = 101

    for k in range(start_epoch, n_epochs):
        train_losses = []
        for i, batch_data in enumerate(trainaset_loader):
            batch_imgs = batch_data[0]

            predicted_imgs, z = ae_model(batch_imgs)

            loss = criterion(predicted_imgs, batch_imgs)
            train_losses.append(loss.item())

            # Zero gradients, perform a backward pass, and update the weights.
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        print 'Finished epoch', k

        plt.figure(figsize=(12, 9))
        plt.title("Loss")
        plt.plot(train_losses, label="total")
        plt.xlabel("Number of times we updated our parameters")
        plt.ylabel("Loss")
        plt.legend()
        plt.savefig(save_root + "loss_{}.jpg".format(k))
        plt.close()

        print k, 'Loss =', sum(train_losses) / len(train_losses)

        if k % 5 == 0:
            torch.save(ae_model.state_dict(), save_root + 'ae_model_{}.pt'.format(k))

            test_losses = []
            directory = save_root + 'test/{}/{}/'.format(latent_size_str, k)
            if not os.path.exists(directory):
                os.makedirs(directory)
            for i, batch_data in enumerate(testset_loader):
                batch_imgs = batch_data[0]

                with torch.no_grad():
                    predicted_imgs, z = ae_model(batch_imgs)

                loss = criterion_te(predicted_imgs, batch_imgs)
                test_losses.append(loss.item())

                # Plotting
                # batch_imgs = invTrans(batch_imgs)
                # predicted_imgs = invTrans(predicted_imgs)
                imgs = torch.stack([batch_imgs, predicted_imgs], dim=1).flatten(0, 1)
                grid = make_grid(imgs, nrow=4, normalize=True, range=(0, 1))
                grid = grid.permute(1, 2, 0)
                plt.figure(figsize=(35, 25))
                # plt.title(f"")
                plt.imshow(grid)
                plt.axis('off')
                # plt.show()
                plt.savefig(directory + 'test_%03d.jpg' % i)
                plt.close()

            print "On test set loss:", sum(test_losses) / len(test_losses)

    print 'Finished Training...'
