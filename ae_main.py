import sys
from ae_test import test_single_image
from ae_train import train
from PIL import Image
from ae_train import create_model

def main():
    argv = sys.argv
    arglen = len(argv)
    print argv, "len:", len(argv)
    if arglen < 2:
        print "USAGE: python " + argv[0] + "  train | test"
        return
        
    latent_size = 64 #You can change this
    
    if argv[1] == 'test':
        frame_address = './data/160_data_c/camera_image_push1_end_7.jpeg' #You can change this
        model_adr = './result_160/8/ae_model_69.pt'  # You can change this
        ae_model = create_model(latent_size, model_adr)
        img = Image.open(frame_address)
        latent_rep = test_single_image(img, ae_model)
        print latent_rep.shape
        print latent_rep

    if argv[1] == 'train':
    	model_adr = None #You can change this
    	start_epoch = 0 #You can change this
        train(latent_size, model_adr, start_epoch)

if __name__ == '__main__':
    main()
